package ru.iteco.taskmanager.bootstrap;

import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.ws.Endpoint;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.service.IDomainService;
import ru.iteco.taskmanager.api.service.IProjectService;
import ru.iteco.taskmanager.api.service.ISessionService;
import ru.iteco.taskmanager.api.service.ITaskService;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.util.HashUtil;

@Getter
@Setter
@Singleton
@NoArgsConstructor
public class Bootstrap {

    @Inject
    private IProjectService projectService;
    @Inject
    private ITaskService taskService;
    @Inject
    private IUserService userService;
    @Inject
    private IDomainService domainService;
    @Inject
    private ISessionService sessionService;

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private IProjectEndpoint projectEndpoint;
    @Inject
    private ITaskEndpoint taskEndpoint;
    @Inject
    private IDomainEndpoint domainEndpoint;
    @Inject
    private ISessionEndpoint sessionEndpoint;

    @NotNull
    public static final String USER_URL = "/UserWebService?wsdl";
    @NotNull
    public static final String PROJECT_URL = "/ProjectWebService?wsdl";
    @NotNull
    public static final String TASK_URL = "/TaskWebService?wsdl";
    @NotNull
    public static final String DOMAIN_URL = "/DomainWebService?wsdl";
    @NotNull
    public static final String SESSION_URL = "/SessionWebService?wsdl";

    private static final String HTTP_PREFIX = "http://localhost:";
    @NotNull
    private static String port = "8080";

    public void init() {
        if (userService.findByLogin("root") == null)
            initUsers();

        @NotNull
        final String tempPort = System.getProperty("server.port");
        if (tempPort != null) {
            port = tempPort;
        }
        initEndpoints();
    }

    private void initEndpoints() {
        Endpoint.publish(HTTP_PREFIX + port + USER_URL, userEndpoint);
        System.out.println(USER_URL);

        Endpoint.publish(HTTP_PREFIX + port + PROJECT_URL, projectEndpoint);
        System.out.println(PROJECT_URL);

        Endpoint.publish(HTTP_PREFIX + port + TASK_URL, taskEndpoint);
        System.out.println(TASK_URL);

        Endpoint.publish(HTTP_PREFIX + port + DOMAIN_URL, domainEndpoint);
        System.out.println(DOMAIN_URL);

        Endpoint.publish(HTTP_PREFIX + port + SESSION_URL, sessionEndpoint);
        System.out.println(SESSION_URL);
    }

    private void initUsers() {

        @NotNull
        String tempUuid = UUID.randomUUID().toString();

        @NotNull
        User user = new User();
        user.setId(tempUuid);
        user.setEmail("root@root.com");
        user.setFirstName("Root");
        user.setLastName("Root");
        user.setMiddleName("Root");
        user.setLogin("root");
        user.setPasswordHash(HashUtil.getHash("1234"));
        user.setPhone("777-77-77");
        user.setRoleType(RoleType.ADMIN);
        userService.persist(user);
    }

}
