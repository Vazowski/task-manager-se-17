package ru.iteco.taskmanager.command.system;

import javax.inject.Singleton;

import ru.iteco.taskmanager.command.AbstractCommand;

@Singleton
public class ExitCommand extends AbstractCommand {

    @Override
    public String command() {
	return "exit";
    }

    @Override
    public String description() {
	return "  -  exit the programm";
    }

    @Override
    public void execute() throws Exception {
	System.exit(0);
    }
}
