package ru.iteco.taskmanager.command.project.find;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.ProjectDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Singleton
public class ProjectFindAllByDescriptionPartCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private IProjectEndpoint projectEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
        return "project-find-all-description-part";
    }

    @Override
    public String description() {
        return "  -  find all project by part of description";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
        if (sessionDTO == null)
            return;
        @Nullable final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
        if (userDTO == null)
            return;

        System.out.print("Part of description of project: ");
        @NotNull final String partOfDescription = scanner.nextLine();
        @Nullable final List<Project> projectList = ProjectDTOConvertUtil
                .DTOsToProjects(projectEndpoint.findAllProjectByPartOfDescription(sessionDTO, partOfDescription));
        if (projectList == null) {
            System.out.println("Empty");
            return;
        }
        for (int i = 0, j = 1; i < projectList.size(); i++) {
            if (projectList.get(i).getOwnerId().equals(userDTO.getId())) {
                System.out.println("[Project " + (j++) + "]");
                System.out.println("ID: " + projectList.get(i).getId());
                System.out.println("OwnerID: " + projectList.get(i).getOwnerId());
                System.out.println("Name: " + projectList.get(i).getName());
                System.out.println("Description: " + projectList.get(i).getDescription());
                System.out.println("DateCreated: " + projectList.get(i).getDateCreated());
                System.out.println("DateBegin: " + projectList.get(i).getDateBegin());
                System.out.println("DateEnd: " + projectList.get(i).getDateEnd());
                System.out.println("Status: " + projectList.get(i).getReadinessStatus().toString());
            }
        }
    }

}
