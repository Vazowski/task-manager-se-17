package ru.iteco.taskmanager.util.convert;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;

public class SessionDTOConvertUtil {

    @Nullable
    public static SessionDTO sessionToDTO(@Nullable final Session session) {
	if (session == null)
	    return null;

	@NotNull
	final SessionDTO sessionDTO = new SessionDTO();
	sessionDTO.setId(session.getId());
	sessionDTO.setTimestamp(session.getTimestamp().toString());
	sessionDTO.setUserId(session.getUserId());
	sessionDTO.setSignature(session.getSignature());
	return sessionDTO;
    }

    @Nullable
    public static Session DTOToSession(@Nullable final SessionDTO sessionDTO) {
	if (sessionDTO == null)
	    return null;

	@NotNull
	final Session session = new Session();
	session.setId(sessionDTO.getId());
	session.setTimestamp(sessionDTO.getTimestamp());
	session.setUserId(sessionDTO.getUserId());
	session.setSignature(sessionDTO.getSignature());
	return session;
    }
}
