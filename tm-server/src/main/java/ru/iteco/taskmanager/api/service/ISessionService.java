package ru.iteco.taskmanager.api.service;

import ru.iteco.taskmanager.entity.Session;

public interface ISessionService {

    void merge(final Session session);

    void persist(final Session session);

    Session findById(final String id);

    void remove(final String id);
}
