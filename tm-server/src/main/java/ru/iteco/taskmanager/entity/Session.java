package ru.iteco.taskmanager.entity;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Session extends AbstractEntity {

    @Column
    private String timestamp;

    @Column
    private String signature;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;
}