package ru.iteco.taskmanager.command.project.update;

import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.ProjectDTO;
import ru.iteco.taskmanager.api.endpoint.ReadinessStatus;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.DateUtil;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Singleton
public class ProjectMergeCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private IProjectEndpoint projectEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
	return "project-merge";
    }

    @Override
    public String description() {
	return "  -  update the project";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;
	@Nullable
	final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
	if (userDTO == null)
	    return;

	System.out.print("Name of project: ");
	@NotNull
	final String inputName = scanner.nextLine();
	System.out.print("Description of project: ");
	@NotNull
	final String inputDescription = scanner.nextLine();
	System.out.print("Date of begining project: ");
	@NotNull
	final String dateBegin = scanner.nextLine();
	System.out.print("Date of ending project: ");
	@NotNull
	final String dateEnd = scanner.nextLine();
	@Nullable
	final ProjectDTO tempProjectDTO = projectEndpoint.findProjectByName(sessionDTO, inputName);

	@Nullable
	ProjectDTO projectDTO = new ProjectDTO();
	projectDTO.setOwnerId(userDTO.getId());
	projectDTO.setName(inputName);
	projectDTO.setDescription(inputDescription);
	projectDTO.setDateBegin(dateBegin);
	projectDTO.setDateEnd(dateEnd);
	projectDTO.setReadinessStatus(ReadinessStatus.PLANNED);

	if (tempProjectDTO == null) {
	    String id = UUID.randomUUID().toString();
	    projectDTO.setDateCreated(DateUtil.getDate(new Date().toString()));
	    projectDTO.setId(id);
	    projectEndpoint.projectMerge(sessionDTO, projectDTO);
	    System.out.println("Done");
	    return;
	}

	if (projectDTO.getOwnerId() == userDTO.getId()) {
	    projectDTO.setId(projectDTO.getId());
	    projectEndpoint.projectMerge(sessionDTO, projectDTO);
	    System.out.println("Done");
	    return;
	}

	System.out.println("Project create other user");
    }
}
