package ru.iteco.taskmanager.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.NoResultException;

import lombok.Getter;
import lombok.Setter;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.repository.UserRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

@Getter
@Setter
@Singleton
@Transactional
@NoArgsConstructor
public class UserService extends AbstractService implements IUserService {

    @Inject
    private UserRepository userRepository;

    public void merge(@NotNull final User user) {
		assert userRepository != null;
		userRepository.merge(user);
    }

    public void persist(@NotNull final User user) {
		assert userRepository != null;
		userRepository.persist(user);
    }

    public User findById(@NotNull final String id) {
        try {
            return userRepository.findById(id);
        } catch (NoResultException e) {
            return null;
        }
    }

    public User findByLogin(@NotNull final String login) {
        try {
            return userRepository.findByLogin(login);
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<User> findAll() {
		assert userRepository != null;
		return userRepository.findAll();
    }
}