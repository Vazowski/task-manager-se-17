package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.taskmanager.api.service.IDomainService;
import ru.iteco.taskmanager.api.service.IProjectService;
import ru.iteco.taskmanager.api.service.ITaskService;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.dto.DomainDTO;
import ru.iteco.taskmanager.dto.ProjectDTO;
import ru.iteco.taskmanager.dto.SessionDTO;
import ru.iteco.taskmanager.dto.TaskDTO;
import ru.iteco.taskmanager.dto.UserDTO;
import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.util.convert.ProjectDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.TaskDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.UserDTOConvertUtil;

@Getter
@Setter
@Singleton
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.IDomainEndpoint")
public class DomainEndpoint implements IDomainEndpoint {

    @Inject
    private IUserService userService;
    @Inject
    private IProjectService projectService;
    @Inject
    private ITaskService taskService;
    @Inject
    private IDomainService domainService;

    @WebMethod
    public DomainDTO saveData(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(userService.findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	@Nullable
	DomainDTO domain = makeDomain();
	return domainService.saveData(domain);

    }

    @WebMethod
    @Nullable
    public DomainDTO loadData(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(userService.findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	return domainService.loadData();
    }

    @WebMethod
    @Nullable
    public DomainDTO jaxbSaveXml(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(userService.findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	@Nullable
	DomainDTO domain = makeDomain();
	return domainService.jaxbSaveXml(domain);
    }

    @WebMethod
    @Nullable
    public DomainDTO jaxbLoadXml(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(userService.findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	return domainService.jaxbLoadXml();
    }

    @WebMethod
    @Nullable
    public DomainDTO jaxbSaveJson(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(userService.findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	@Nullable
	DomainDTO domain = makeDomain();
	return domainService.jaxbSaveJson(domain);
    }

    @WebMethod
    @Nullable
    public DomainDTO jaxbLoadJson(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(userService.findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	return domainService.jaxbLoadJson();
    }

    @WebMethod
    public DomainDTO jacksonSaveXml(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO)
	    throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(userService.findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	@Nullable
	DomainDTO domain = makeDomain();
	return domainService.jacksonSaveXml(domain);
    }

    @WebMethod
    @Nullable
    public DomainDTO jacksonLoadXml(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO)
	    throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(userService.findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	return domainService.jacksonLoadXml();
    }

    @WebMethod
    public DomainDTO jacksonSaveJson(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO)
	    throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(userService.findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	@Nullable
	DomainDTO domain = makeDomain();
	return domainService.jacksonSaveJson(domain);
    }

    @WebMethod
    @Nullable
    public DomainDTO jacksonLoadJson(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO)
	    throws Exception {
	@NotNull
	final String userId = sessionDTO.getUserId();
	@NotNull
	final UserDTO user = UserDTOConvertUtil.userToDTO(userService.findById(userId));
	if (user == null)
	    return null;
	if (!user.getRoleType().equals(RoleType.ADMIN))
	    return null;

	return domainService.jacksonLoadJson();
    }

    private DomainDTO makeDomain() {
	@NotNull
	DomainDTO domain = new DomainDTO();
	@Nullable
	final List<UserDTO> userList = UserDTOConvertUtil.usersToDTO(userService.findAll());
	if (userList != null)
	    domain.setUserList(userList);
	@Nullable
	final List<ProjectDTO> projectList = ProjectDTOConvertUtil.projectsToDTO(projectService.findAll());
	if (projectList != null)
	    domain.setProjectList(projectList);
	@Nullable
	final List<TaskDTO> taskList = TaskDTOConvertUtil.tasksToDTO(taskService.findAll());
	if (taskList != null)
	    domain.setTaskList(taskList);

	return domain;
    }
}
