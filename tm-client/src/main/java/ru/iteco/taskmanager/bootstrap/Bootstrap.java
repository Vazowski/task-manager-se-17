package ru.iteco.taskmanager.bootstrap;

import java.util.Scanner;
import java.util.Set;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.ITerminalService;
import ru.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.command.AbstractCommand;

@Getter
@Setter
@Singleton
@NoArgsConstructor
public class Bootstrap {

    @NotNull
    private Scanner scanner;
    @NotNull
    private String inputCommand;

    @Inject
    private ITerminalService terminalService;
    @Inject
    private ISessionService sessionService;

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private IProjectEndpoint projectEndpoint;
    @Inject
    private ITaskEndpoint taskEndpoint;
    @Inject
    private ISessionEndpoint sessionEndpoint;
    @Inject
    private IDomainEndpoint domainEndpoint;

    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.iteco.taskmanager")
            .getSubTypesOf(AbstractCommand.class);

    public void init() {
        scanner = new Scanner(System.in);
        try {
            registerCommand(classes);
            terminalService.get("login").execute();
            System.out.println("Enter command (type help for more information)");

            while (true) {
                System.out.print("> ");
                inputCommand = scanner.nextLine();
                if (!terminalService.getCommands().containsKey(inputCommand)) {
                    System.out.println("Command doesn't exist");
                } else {
                    terminalService.get(inputCommand).execute();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerCommand(@Nullable final Set<Class<? extends AbstractCommand>> classes) throws Exception {
        if (classes == null)
            return;
        for (@Nullable final Class clazz : classes) {
            if (clazz == null || !AbstractCommand.class.isAssignableFrom(clazz))
                continue;
            @NotNull
            AbstractCommand command = (AbstractCommand) CDI.current().select(clazz).get();
            registerCommand(command);
        }
    }

    private void registerCommand(@Nullable final AbstractCommand command) {
        if (command == null)
            return;
        terminalService.put(command.command(), command);
    }
}
