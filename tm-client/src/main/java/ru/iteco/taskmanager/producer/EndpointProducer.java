package ru.iteco.taskmanager.producer;

import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

import ru.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.endpoint.DomainEndpointService;
import ru.iteco.taskmanager.endpoint.ProjectEndpointService;
import ru.iteco.taskmanager.endpoint.SessionEndpointService;
import ru.iteco.taskmanager.endpoint.TaskEndpointService;
import ru.iteco.taskmanager.endpoint.UserEndpointService;

public class EndpointProducer {

    @Produces
    @Singleton
    public IUserEndpoint userEndpoint() {
	return new UserEndpointService().getUserEndpointPort();
    }

    @Produces
    @Singleton
    public IDomainEndpoint domainEndpoint() {
	return new DomainEndpointService().getDomainEndpointPort();
    }

    @Produces
    @Singleton
    public IProjectEndpoint projectEndpoint() {
	return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Produces
    @Singleton
    public ITaskEndpoint taskEndpoint() {
	return new TaskEndpointService().getTaskEndpointPort();
    }

    @Produces
    @Singleton
    public ISessionEndpoint sessionEndpoint() {
	return new SessionEndpointService().getSessionEndpointPort();
    }

}
