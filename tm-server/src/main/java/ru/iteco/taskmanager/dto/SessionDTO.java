package ru.iteco.taskmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SessionDTO extends AbstractDTO {

    private String timestamp;

    private String userId;

    private String signature;
}
