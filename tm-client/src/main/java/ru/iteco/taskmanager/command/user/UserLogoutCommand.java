package ru.iteco.taskmanager.command.user;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.ITerminalService;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Singleton
public class UserLogoutCommand extends AbstractCommand {

    @Inject
    private ISessionService sessionService;
    @Inject
    private ITerminalService terminalService;
    @Inject
    private ISessionEndpoint sessionEndpoint;

    @Override
    public String command() {
	return "logout";
    }

    @Override
    public String description() {
	return "  -  user logout";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;
	sessionEndpoint.removeSession(sessionDTO);
	sessionService.setSession(null);
	System.out.println("Done");
	terminalService.get("login").execute();
    }

}
