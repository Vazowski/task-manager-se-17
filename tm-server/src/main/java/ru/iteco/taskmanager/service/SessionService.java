package ru.iteco.taskmanager.service;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.ISessionService;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.repository.SessionRepository;

@Getter
@Setter
@Singleton
@Transactional
@NoArgsConstructor
public class SessionService extends AbstractService implements ISessionService {

    @Inject
    private SessionRepository sessionRepository;

    public void merge(@Nullable final Session session) {
    	sessionRepository.merge(session);
    }

    public void persist(@Nullable final Session session) {
    	sessionRepository.persist(session);
    }

    @Nullable
    public Session findById(@Nullable final String id) {
    	return sessionRepository.findById(id);
    }

    public void remove(@NotNull final String id) {
		sessionRepository.remove(findById(id));
	}
}
