package ru.iteco.taskmanager.util.convert;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.dto.UserDTO;
import ru.iteco.taskmanager.entity.User;

public class UserDTOConvertUtil {

    @Nullable
    public static UserDTO userToDTO(@Nullable final User user) {
	if (user == null)
	    return null;

	@NotNull
	final UserDTO userDTO = new UserDTO();
	userDTO.setId(user.getId());
	userDTO.setEmail(user.getEmail());
	userDTO.setFirstName(user.getFirstName());
	userDTO.setLastName(user.getLastName());
	userDTO.setMiddleName(user.getMiddleName());
	userDTO.setLogin(user.getLogin());
	userDTO.setPasswordHash(user.getPasswordHash());
	userDTO.setPhone(user.getPhone());
	userDTO.setRoleType(user.getRoleType());
	return userDTO;
    }

    @Nullable
    public static List<UserDTO> usersToDTO(@Nullable final List<User> listUser) {
	if (listUser == null || listUser.isEmpty())
	    return null;
	@NotNull
	final List<UserDTO> listUsersDTO = new ArrayList<>();
	for (User user : listUser) {
	    listUsersDTO.add(userToDTO(user));
	}
	return listUsersDTO;
    }

    @Nullable
    public static User DTOToUser(@Nullable final UserDTO userDTO) {
	if (userDTO == null)
	    return null;

	@NotNull
	final User user = new User();
	user.setId(userDTO.getId());
	user.setEmail(userDTO.getEmail());
	user.setFirstName(userDTO.getFirstName());
	user.setLastName(userDTO.getLastName());
	user.setMiddleName(userDTO.getMiddleName());
	user.setLogin(userDTO.getLogin());
	user.setPasswordHash(userDTO.getPasswordHash());
	user.setPhone(userDTO.getPhone());
	user.setRoleType(userDTO.getRoleType());
	return user;
    }

    @Nullable
    public static List<User> DTOsToUsers(@Nullable final List<UserDTO> listUserDTOs) {
	if (listUserDTOs == null || listUserDTOs.isEmpty())
	    return null;
	@NotNull
	final List<User> listUsers = new ArrayList<>();
	for (UserDTO userDTO : listUserDTOs) {
	    listUsers.add(DTOToUser(userDTO));
	}
	return listUsers;
    }
}
